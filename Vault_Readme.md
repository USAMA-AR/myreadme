# Installing Vault On Ubuntu
```
# curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
# sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
# sudo apt-get update && sudo apt-get install vault
```
### Verify Installation
```
# vault
```
### Starting the Dev Server
```
# vault server -dev
```
### Now Open another new terminal 
```
# export VAULT_ADDR='http://127.0.0.1:8200'
# export VAULT_TOKEN=PASTE YOUR ROOT TOKEN.YOU GET TOKEN FROM 
 STARTING THE DEV SERVER LIKE ="s.XmpNPoi9sRhYtdKHaQhkHP6x"
```
### Verify the server is running
```
# vault status
```
### Enable the AWS secrets engine
```
# vault secrets enable -path=aws aws
```
### Configure the AWS secrets engine
### ADD ACCESS & SECRET KEYS OF AWS USER ACCOUNT
```
# vault write aws/config/root \
    access_key=$AWS_ACCESS_KEY_ID \
    secret_key=$AWS_SECRET_ACCESS_KEY \
    region=us-east-1
```
### Configure a role
```
# vault write aws/roles/my-role \
        credential_type=iam_user \
        policy_document=-<<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1426528957000",
      "Effect": "Allow",
      "Action": [
        "ec2:*"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF\
```
### Generate the secret
```
# vault read aws/creds/my-role

YOU GOT LIKE THIS:
Key                Value
---                -----
lease_id           aws/creds/my-role/0bce0782-32aa-25ec-f61d-c026ff22106e
lease_duration     768h
lease_renewable    true
access_key         AKIAJELUDIANQGRXCTZQ
secret_key         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
security_token     <nil>
```
### Vault will automatically revoke this credential after 768 ### hours,so after 768hrs you need revoke the secret

### Revoke the secret
```
# vault lease revoke aws/creds/my-role/0bce0782-32aa-25ec-f61d-c026ff22106
```

# Installing Terraform on Ubuntu
```
 # sudo apt-get update -y
 ```
 ### Download wget and unzip
 ```
 # sudo apt-get install wget unzip -y
 ```
 ### Download Terraform package 
 ```
 # sudo wget https://releases.hashicorp.com/terraform/0.12.18/terraform_0.12.18_linux_amd64.zip
 # sudo unzip terraform_0.12.18_linux_amd64.zip
 ```
 ### Move terraform to bin folder
 ```
 sudo mv terraform /usr/local/bin/
 ```
 ### Verify Teraform version
 ```
 terraform -v
 ```
 ### Now make a resources file in terraform
 ```
 # sudo su
 # mkdir terraformfiles
 # cd terrafromfiles
 # nano main.tf

 PASTE IT & SAVE IT

 terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.58.0"
    }
  }
}

data "vault_generic_secret" "aws_creds" {
	path = "secret/aws"
}

provider "aws" {
  region  = data.vault_generic_secret.aws_creds.data["region"]
	access_key = data.vault_generic_secret.aws_creds.data["aws_access_key_id"]
	secret_key = data.vault_generic_secret.aws_creds.data["aws_secret_access_key"]
}


module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "application-vpc"
  cidr = "10.0.0.0/16"

  azs            = ["${var.region}a"]
  public_subnets = ["10.0.101.0/24"]

  tags = {
    createdBy = "<%=username%>"
  }
}

resource "random_string" "random" {
  length           = 3
  special          = true
  override_special = "/@£$"
}

resource "aws_key_pair" "app_ssh" {
  key_name   = "application-ssh-${random_string.random.result}"
  public_key = var.instance_ssh_public_key
  tags = {
    Name      = "application-ssh"
    createdBy = "listentolearn"
  }
}

resource "aws_instance" "app_vm" {
  # Amazon Linux 2 AMI (HVM), SSD Volume Type
  ami                         = var.ami
  instance_type               = "t2.micro"
  subnet_id                   = module.vpc.public_subnets[0]
  vpc_security_group_ids      = [aws_security_group.vm_sg.id]
  key_name                    = aws_key_pair.app_ssh.key_name
  associate_public_ip_address = false

  tags = {
    Name      = "application-vm"
    createdBy = "listentolearn"
  }
}

resource "aws_eip" "elastic_ip" {
  instance = aws_instance.app_vm.id
  vpc      = true
}

resource "aws_security_group" "vm_sg" {
  name        = "vm-security-group"
  description = "Allow incoming connections."

  vpc_id = module.vpc.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${local.my_ip.ip}/32"]
  }

  # application
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
```
# Installing Ansible On Ubuntu
```
# sudo apt-add-repository ppa:ansible/ansible
# sudo apt-get update
# sudo apt-get install ansible -y
```
### Because Ansible requires a Python interpreter.we need to install Python as well.
```
# sudo apt-get install python -y
```
### Configure SSH access to the server
```
# ssh-keygen
```
### Display the contents of the public SSH key with the command
```
# cat ~/.ssh/id_rsa.pub
```
### Copy the text from the key.
### Log into your node server.
### Issue the command sudo -s.
### Open the authorized_keys file with the command sudo nano ~/.ssh/authorized_keys.
### Paste the contents of the server key at the bottom of this file.
### Save and close the file.


### If you want to simplify this process, issue the command (from the Ansible server):
```
# ssh-copy-id NODE_IP
```
### Add Inventory file.yml
```
all:
  hosts:
    0.0.0.0:
      ansible_ssh_extra_args: -o StrictHostKeyChecking=no
      ansible_ssh_private_key_file: ./secrets/ssh.private
      ansible_user: ec2-user

```

### Setting up our node
```
# sudo nano /etc/ansible/hosts
[group_name]
ALIAS NODE_IP
# ansible -m ping all
```

### Create a new directory (on the Ansible server) with the command sudo mkdir /etc/ansible/group_vars.
### Create a new file with the command sudo nano /etc/ansible/group_vars/servers.
### In that file, add the following line: ansible_ssh_user: USERNAME (Where USERNAME is the username on the remote node).
### Save and close that file.

### Now Create Playbook
```
# mkdir ansibleplaybook.yml


sudo su
su - ansible 
mkdir ~/kube-cluster
cd ~/kube-cluster
nano ~/kube-cluster/hosts

	// Edit in hosts file
		[masters]
		master ansible_host=master_ip ansible_user=root
		
		[workers]
		worker1 ansible_host=worker_1_ip ansible_user=root
		worker2 ansible_host=worker_2_ip ansible_user=root
		
		[all:vars]
		ansible_python_interpreter=/usr/bin/python3
	// End Edit

nano ~/kube-cluster/initial.yml

	// Edit in initial file
		- hosts: all
		  become: yes
		  tasks:
		    - name: create the 'ubuntu' user
		      user: name=ubuntu append=yes state=present createhome=yes shell=/bin/bash
		
		    - name: allow 'ubuntu' to have passwordless sudo
		      lineinfile:
		        dest: /etc/sudoers
		        line: 'ubuntu ALL=(ALL) NOPASSWD: ALL'
		        validate: 'visudo -cf %s'
		
		    - name: set up authorized keys for the ubuntu user
		      authorized_key: user=ubuntu key="{{item}}"
		      with_file:
		        - ~/.ssh/id_rsa.pub
	// End Edit

ansible-playbook -i hosts ~/kube-cluster/initial.yml
nano ~/kube-cluster/kube-dependencies.yml

	// Edit in kub-dependencies file
		- hosts: all
		  become: yes
		  tasks:
		   - name: install Docker
		     apt:
		       name: docker.io
		       state: present
		       update_cache: true
		
		   - name: install APT Transport HTTPS
		     apt:
		       name: apt-transport-https
		       state: present
		
		   - name: add Kubernetes apt-key
		     apt_key:
		       url: https://packages.cloud.google.com/apt/doc/apt-key.gpg
		       state: present
		
		   - name: add Kubernetes' APT repository
		     apt_repository:
		      repo: deb http://apt.kubernetes.io/ kubernetes-xenial main
		      state: present
		      filename: 'kubernetes'
		
		   - name: install kubelet
		     apt:
		       name: kubelet=1.14.0-00
		       state: present
		       update_cache: true
		
		   - name: install kubeadm
		     apt:
		       name: kubeadm=1.14.0-00
		       state: present
		
		- hosts: master
		  become: yes
		  tasks:
		   - name: install kubectl
		     apt:
		       name: kubectl=1.14.0-00
		       state: present
		       force: yes
	// End Edit

ansible-playbook -i hosts ~/kube-cluster/kube-dependencies.yml
nano ~/kube-cluster/master.yml

	// Edit in master file
		- hosts: master
		  become: yes
		  tasks:
		    - name: initialize the cluster
		      shell: kubeadm init --pod-network-cidr=10.244.0.0/16 >> cluster_initialized.txt
		      args:
		        chdir: $HOME
		        creates: cluster_initialized.txt
		
		    - name: create .kube directory
		      become: yes
		      become_user: ubuntu
		      file:
		        path: $HOME/.kube
		        state: directory
		        mode: 0755
		
		    - name: copy admin.conf to user's kube config
		      copy:
		        src: /etc/kubernetes/admin.conf
		        dest: /home/ubuntu/.kube/config
		        remote_src: yes
		        owner: ubuntu
		
		    - name: install Pod network
		      become: yes
		      become_user: ubuntu
		      shell: kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/a70459be0084506e4ec919aa1c114638878db11b/Documentation/kube-flannel.yml >> pod_network_setup.txt
		      args:
		        chdir: $HOME
		        creates: pod_network_setup.txt
	// End Edit

ansible-playbook -i hosts ~/kube-cluster/master.yml
ssh ubuntu@master_ip
	// Once inside the master node, execute:
kubectl get nodes
nano ~/kube-cluster/workers.yml

	// Edit in workers file
		- hosts: master
		  become: yes
		  gather_facts: false
		  tasks:
		    - name: get join command
		      shell: kubeadm token create --print-join-command
		      register: join_command_raw
		
		    - name: set join command
		      set_fact:
		        join_command: "{{ join_command_raw.stdout_lines[0] }}"
		
		
		- hosts: workers
		  become: yes
		  tasks:
		    - name: join cluster
		      shell: "{{ hostvars['master'].join_command }} >> node_joined.txt"
		      args:
		        chdir: $HOME
		        creates: node_joined.txt
	// End Edit

ansible-playbook -i hosts ~/kube-cluster/workers.yml
ssh ubuntu@master_ip
kubectl get nodes
